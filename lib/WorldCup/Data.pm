package WorldCup::Data;

use strict;
use warnings;

use Data::Dump qw(dump);
use DateTime::Format::ISO8601;
use DateTime::Format::Flexible;
use File::Slurp;
use JSON::XS;
use LWP::Simple;

sub new {
    my ($pkg, $config) = @_;

    my $self = bless {
        config => $config,
    }, $pkg;

    unless ($self->refresh()) {
        $self->_build_data();
    }

    return $self;
}

sub refresh {
    my $self = shift;

    if (mirror($self->{config}->{data_url}, $self->{config}->{data_path}) != RC_NOT_MODIFIED) {
        delete $self->{data};
        $self->_build_data();
        return 1;
    }
}

sub _get_draw {
    my $self = shift;
    $self->{draw} ||= decode_json(read_file($self->{config}->{draw_path}));
    return $self->{draw};
}

sub _get_data {
    my $self = shift;
    $self->{data} ||= decode_json(read_file($self->{config}->{data_path}));
    return $self->{data};
}

sub _build_data {
    my $self = shift;

    my $draw = $self->_get_draw();
    my $data = $self->_get_data();

    for my $key (qw(teams_by_id teams_by_code teams_by_player channels_by_id groups fixtures)) {
        delete $self->{$key};
    }

    for my $team (@{ $data->{teams} }) {
        $team->{player} = $draw->{$team->{fifaCode}};

        for my $key (qw(won drawn lost gd pts)) {
            $team->{$key} ||= 0;
        }

        $self->{teams_by_id}->{$team->{id}}         = $team;
        $self->{teams_by_code}->{$team->{fifaCode}} = $team;
        push @{ $self->{teams_by_player}->{$team->{player}} }, $team;
    }

    for my $channel (@{ $data->{tvchannels} }) {
        if ($channel->{name} =~ /^(.*) UK$/) {
            $channel->{name} = $1;
        }
        $self->{channels_by_id}->{$channel->{id}} = $channel;
    }

    $self->{groups_completed} = 1;
    for my $group_letter (keys %{ $data->{groups} }) {
        my $group = {
            name => 'Group ' . uc($group_letter),
            winner   => $data->{groups}->{$group_letter}->{winner},
            runnerup => $data->{groups}->{$group_letter}->{runnerup},
        };
        $self->{groups}->{$group_letter} = $group;

        for my $m (@{ $data->{groups}->{$group_letter}->{matches} }) {
            # Store fixture
            push @{ $self->{fixtures} }, $m;
            $self->{groups_completed} &&= $m->{finished};

            # Assign teams to this group
            my $home_team = $self->{teams_by_id}->{$m->{home_team}};
            my $away_team = $self->{teams_by_id}->{$m->{away_team}};
            $group->{teams}->{$home_team->{id}} = $home_team;
            $group->{teams}->{$away_team->{id}} = $away_team;
            $home_team->{group} = $group_letter;
            $away_team->{group} = $group_letter;
        }
    }

    for my $knockout_round (keys %{ $data->{knockout} }) {
        for my $m (@{ $data->{knockout}->{$knockout_round}->{matches} }) {
            push @{ $self->{fixtures} }, $m;
        }
    }

    for my $m (@{ $self->{fixtures} }) {
        # Add name of UK channel to fixture
        ($m->{channel}) = map {
            $self->{channels_by_id}->{$_}->{name}
        } grep {
            $self->{channels_by_id}->{$_}->{country} eq 'UK'
        } @{ $m->{channels} };

        # Update aggregated stats for both teams
        if ($m->{finished}) {
            my $home_team = $self->{teams_by_id}->{$m->{home_team}};
            my $away_team = $self->{teams_by_id}->{$m->{away_team}};
            $home_team->{gd} += $m->{home_result} - $m->{away_result};
            $away_team->{gd} += $m->{away_result} - $m->{home_result};
            if (($m->{type} eq 'qualified' && $m->{winner} == $m->{home_team})
                or ($m->{type} eq 'group' && $m->{home_result} > $m->{away_result})) {
                $home_team->{won}++;
                $away_team->{lost}++;
            } elsif (($m->{type} eq 'qualified' && $m->{winner} == $m->{away_team})
                or ($m->{type} eq 'group' && $m->{home_result} < $m->{away_result})) {
                $home_team->{lost}++;
                $away_team->{won}++;
            } elsif ($m->{type} eq 'group') {
                $home_team->{drawn}++;
                $away_team->{drawn}++;
            } else {
                warn 'Found completed match with no winner: ' . dump($m);
            }
        }
    }

    # Calculate points totals for each team
    for my $team (values %{ $self->{teams_by_id} }) {
        $team->{points} = 3 * $team->{won} + $team->{drawn};
    }

    # Parse fixture date/times and sort into ascending order
    map {
        $_->{date_obj} = DateTime::Format::ISO8601->parse_datetime($_->{date});
        $_->{date_obj}->set_time_zone( 'Europe/London' );
    } @{ $self->{fixtures} };

    $self->{fixtures} = [sort {
        DateTime->compare($a->{date_obj}, $b->{date_obj})
    } @{ $self->{fixtures} }];

    # Link fixtures to actual team references instead of storing IDs
    map {
        $_->{home_team} = $self->{teams_by_id}->{$_->{home_team}};
        $_->{away_team} = $self->{teams_by_id}->{$_->{away_team}};
    } @{ $self->{fixtures} };

    # Determine next fixture for each team
    my $now = DateTime->now();
    for my $f (grep { $_->{date_obj}->compare($now) > 0 } @{ $self->{fixtures} }) {
        $f->{home_team}->{next_fixture} ||= $f;
        $f->{away_team}->{next_fixture} ||= $f;
    }
}

sub fixtures {
    my ($self, $date) = @_;
    my %dow = (mon => 1, tue => 2, wed => 3, thu => 4, fri => 5, sat => 6, sun => 7);
    my $today;

    # Parse desired date from user input
    if (!$date or $date eq 'today') {
        $today = DateTime->today();

    } elsif ($date eq 'tomorrow') {
        $today = DateTime->today()->add(days => 1);

    } elsif ($date eq 'yesterday') {
        $today = DateTime->today()->subtract(days => 1);

    } elsif ($date =~ /(mon|tue|wed|thu|fri|sat|sun)[nes]{0,3}day$/i and my $desired = $dow{$1}) {
        $today = DateTime->today()->add(days => ($desired - DateTime->today()->dow()) % 7);
        
    } else {
        $today   = eval { DateTime::Format::ISO8601->parse_datetime($date) };
        $today ||= eval { DateTime::Format::Flexible->parse_datetime($date) };
        return unless $today;
    }
    my $tomorrow = $today->clone()->add(days => 1);

    # Filter to fixtures on this day and before the next
    my @fixtures = grep {
        DateTime->compare($_->{date_obj}, $today) >= 0 and
        DateTime->compare($_->{date_obj}, $tomorrow) <= 0
    } @{ $self->{fixtures} };

    return {
        date => $today->date(),
        fixtures => \@fixtures,
    };
}

1;

